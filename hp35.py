def hp35(lst):
    total = lst[0]
    length = len(lst)
    i = 2
    while True: 
        if i > length:
            break
        elif lst[i] == "add":
            total += lst[i - 1]
        elif lst[i] == "sub":
            total -= lst[i - 1]
        i += 2
    print(total)


lst = [2, 2, "add", 1, "sub", 5, "sub"]
hp35(lst)
